<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patch extends Model
{
    protected $fillable = [
        'id',
        'patch',
        'patch_url',
        'name',
        'banner',
        'is_expansion',
        'date'
    ];

    public $timestamps = false;

    public function actions()
    {
        return $this->hasMany(Action::class);
    }
}
