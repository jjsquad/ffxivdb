<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Action;
use App\Classjob;
use App\Patch;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Viion\Lodestone\LodestoneAPI;

Route::get('/macro', function () {

    return view('actions');

});

Route::get('/', function(){

    $api = new LodestoneAPI();
    $fc_id = '9234349560946502404';

    $fc = $api->Search->Freecompany($fc_id);

    return view('welcome', ['fc'=>$fc]);

});

Route::get('character/search/{name}', function($name){


    $api = new LodestoneAPI();

    $results = $api->Search->Character($name);

    return view('character.list', compact('results'));

});

Route::get('character/profile/{id}', function($id){


    $api = new LodestoneAPI();

    $char = $api->Search->Character($id);

    return view('character.profile', ['char'=>$char]);

});



Route::get('/import/actions/{start}/{end}', function($start, $end){

    $client = new \GuzzleHttp\Client();

    $result = [];

    for($i = $start; $i <= $end; $i++)
    {
        $response = $client->get('http://api.xivdb.com/action/'. $i);
        $action = $response->getBody()->getContents();
        $action_json = json_decode($action);

        if(empty(object_get($action_json,'error')))
        {
            if(is_object($action_json->classjob))
            {
                $classjob = $action_json->classjob;
                $action_json->classjob_id = $classjob->id;
                $action_json->classjob = json_encode($action_json->classjob);

                if(empty(Classjob::find($classjob->id)))
                {
                    Classjob::create(json_decode(json_encode($classjob), true));
                }
            }
            else
            {
                $action_json->classjob = 'false';
                $action_json->classjob_id = 0;
            }

            if(is_object($action_json->patch))
            {
                $patch = $action_json->patch;
                $action_json->patch = json_encode($action_json->patch);
                $action_json->patch_id = $patch->id;

                if(empty(Patch::find($patch->id)))
                {
                    Patch::create(json_decode(json_encode($patch), true));
                }
            }
            else
            {
                $action_json->patch = 'false';
                $action_json->patch_id = 0;
            }

            $action_json->json = json_encode($action_json->json);

            if(empty(Action::find($action_json->id)))
            {
                $result[] = Action::create(json_decode(json_encode($action_json), true));
            }
        }
    }

    return $result;

});

Route::group(['prefix'=>'api'], function(){

    Route::get('actions/{abbr}', function($abbr){

        $classjob = Classjob::where('abbr', $abbr)->first();


        if($classjob->is_job)
        {
            $actions = Action::where('class_parent', $classjob->actions->first()->class_parent)->orderBy('level','asc')->get();
        }
        else
        {
            $actions = $classjob->actions()->orderBy('level', 'asc')->get();
        }
        return response(compact('actions'), 200);
    });

    Route::get('classjobs', function(){

        $classjobs = Classjob::where('id', '>', 0)->get();

        return response(compact('classjobs'), 200);

    });

});


Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
