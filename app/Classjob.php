<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classjob extends Model
{
    protected $fillable = [
        'id',
        'name',
        'abbr',
        'is_job',
        'icon',
        'patch',
        'icon1',
        'icon2',
        'icon3'
    ];

    public   $timestamps = false;

    public function actions()
    {
        return $this->hasMany(Action::class);
    }

}
