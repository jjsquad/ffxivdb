<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 25/01/2016
 * Time: 02:21
 */

namespace App\Tools;

use Illuminate\Database\Eloquent\Model;
use Mockery\CountValidator\Exception;

class JsonToModel
{
    /**
     * Import from a json file to populate a database table with respective model
     *
     * @param $filename
     * @param Model $model
     * @return array|string
     */
    public static function import($filename, Model $model)
    {
        try{
            $content = file_get_contents($filename);
            $items = array_map(function($item) use ($model){
                return $model->create(json_decode(json_encode($item), true));
            }, json_decode($content));
            return $items;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }
}