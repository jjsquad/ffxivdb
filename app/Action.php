<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [
        'id',
        'name',
        'name_ja',
        'name_en',
        'name_fr',
        'name_de',
        'name_ch',
        'help',
        'json',
        'icon',
        'level',
        'classjob_id',
        'classjob_category',
        'spell_group',
        'can_target_self',
        'can_target_party',
        'can_target_friendly',
        'can_target_hostile',
        'can_target_dead',
        'status_required',
        'status_gain_self',
        'cost',
        'cost_hp',
        'cost_mp',
        'cost_tp',
        'cast_range',
        'cast_time',
        'recast_time',
        'is_in_game',
        'is_trait',
        'is_target_area',
        'action_category',
        'action_combo',
        'action_proc_status',
        'action_timeline_hit',
        'action_timeline_use',
        'action_data',
        'effect_range',
        'patch_id',
        'class_name',
        'class_abbr',
        'class_category',
        'class_parent',
        'class_is_job',
        'url',
        'url_api',
        'url_xivdb',
        'url_xivdb_ja',
        'url_xivdb_fr',
        'url_xivdb_de',
        'url_type',
        'icon_hq',
        'help_html',
        '_cid',
        '_type'
    ];

    public $timestamps = false;

    public function patch()
    {
        return $this->belongsTo(Patch::class);
    }

    public function classjob()
    {
        return $this->belongsTo(Classjob::class);
    }
}
