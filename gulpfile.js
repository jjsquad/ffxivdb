var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.scripts([
        '../../../node_modules/jquery/dist/jquery.min.js',
        '../../../node_modules/vue/dist/vue.min.js',
        '../../../node_modules/vue-resource/dist/vue-resource.min.js',
        '../../../node_modules/sweetalert/dist/sweetalert.min.js'
    ], 'public_html/js/libraries.js');

    mix.scripts([
        'ffxiv.js'
    ], 'public_html/js/ffxiv.js');

    mix.styles([
        'ffxiv.css',
        'finalfantasyxiv.css',
        'eurostile-font.css',
        '../../../node_modules/sweetalert/dist/sweetalert.css'
    ], 'public_html/css/ffxiv.css');

    mix.copy('resources/assets/images', 'public_html/images');
    mix.copy('resources/assets/fonts', 'public_html/fonts');

});
