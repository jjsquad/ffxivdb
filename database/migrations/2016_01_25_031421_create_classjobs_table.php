<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classjobs', function (Blueprint $table) {
            $table->integer('id')->index()->unsigned();
            $table->string('name')->nullable();
            $table->string('abbr', 3)->default('NNN');
            $table->boolean('is_job')->default(0);
            $table->string('icon')->nullable();
            $table->integer('patch')->default(0);
            $table->string('icon1')->nullable();
            $table->string('icon2')->nullable();
            $table->string('icon3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('classjobs');
    }
}
