<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->integer('id')->index()->unsigned();
            $table->string('name')->nullable();
            $table->string('name_ja')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_fr')->nullable();
            $table->string('name_de')->nullable();
            $table->string('name_ch')->nullable();
            $table->string('help')->nullable();
            $table->string('json')->default('[]');
            $table->string('icon')->nullable();
            $table->integer('level')->nullable();
            $table->integer('classjob_id')->index()->unsigned();
            $table->integer('classjob_category')->nullable();
            $table->integer('spell_group')->nullable();
            $table->boolean('can_target_self')->nullable();
            $table->boolean('can_target_party')->nullable();
            $table->boolean('can_target_friendly')->nullable();
            $table->boolean('can_target_hostile')->nullable();
            $table->boolean('can_target_dead')->nullable();
            $table->integer('status_required')->nullable();
            $table->integer('status_gain_self')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('cost_hp')->nullable();
            $table->integer('cost_mp')->nullable();
            $table->integer('cost_tp')->nullable();
            $table->integer('cast_range')->nullable();
            $table->integer('cast_time')->nullable();
            $table->integer('recast_time')->nullable();
            $table->boolean('is_in_game')->nullable();
            $table->boolean('is_trait')->nullable();
            $table->boolean('is_target_area')->nullable();
            $table->integer('action_category')->nullable();
            $table->integer('action_combo')->nullable();
            $table->integer('action_proc_status')->nullable();
            $table->integer('action_timeline_hit')->nullable();
            $table->integer('action_timeline_use')->nullable();
            $table->integer('action_data')->nullable();
            $table->integer('effect_range')->nullable();
            $table->integer('patch_id')->index()->unsigned();
            $table->string('class_name')->nullable();
            $table->string('class_abbr', 3)->nullable();
            $table->integer('class_category')->nullable();
            $table->integer('class_parent')->nullable();
            $table->boolean('class_is_job')->nullable();
            $table->string('url')->nullable();
            $table->string('url_api')->nullable();
            $table->string('url_xivdb')->nullable();
            $table->string('url_xivdb_ja')->nullable();
            $table->string('url_xivdb_fr')->nullable();
            $table->string('url_xivdb_de')->nullable();
            $table->string('url_type')->nullable();
            $table->string('icon_hq')->nullable();
            $table->string('help_html')->nullable();
            $table->integer('_cid')->nullable();
            $table->string('_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actions');
    }
}
