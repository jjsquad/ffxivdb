<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patches', function (Blueprint $table) {
            $table->integer('id')->index()->unsigned();
            $table->string('patch')->nullable();
            $table->string('patch_url')->nullable();
            $table->string('name')->nullable();
            $table->string('banner')->nullable();
            $table->boolean('is_expansion')->default(0);
            $table->date('date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patches');
    }
}
