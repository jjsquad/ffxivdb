/**
 * BG SCROLL
 */
;(function($){
    // Crystal Scroll
    $('#wrapper').css({'display':'block', 'opacity':'0'});
    $('#wrapper').delay(800).animate({'opacity':'1'}, 500, 'linear', function(){
        var posY1 = 0;
        var posY2 = 0;
        var posY3 = 0;
        var imgH = 500;
        setInterval(function(){
            if (posY1 <= -900) posY1 = 0;
            if (posY2 <= -900) posY2 = 0;
            if (posY3 <= -1200) posY3 = 0;
            posY1 -= 1;
            posY2 -= 2;
            posY3 -= 3;
            $('#crystal_01').css({ backgroundPosition: '0' + posY1 + 'px' });
            $('#crystal_02').css({ backgroundPosition: '0' + posY2 + 'px' });
            $('#crystal_03').css({ backgroundPosition: '0' + posY3 + 'px' });
        },50);
    });
})(jQuery);

/* ------------------------------------------ */
new Vue({
    el: '#actionsApp',

    data: {
        actions: [],
        classjobs: [],
        currentSelected: '',
        selected: [],
        totalItems: 0,
        tpcost: 0,
        macros: []
    },

    methods: {

        load: function() {

            var self = this;

            self.$http.get('/api/classjobs', function(response){
                self.$set('classjobs', response.classjobs);
            });
        },

        selectClass: function(classjob) {
            var self = this;

            self.$set('currentSelected', classjob.name);

            self.$http.get('/api/actions/'+ classjob.abbr, function(response){

                self.$set('actions', response.actions);

            });
        },

        getQueryVars: function(variable){

            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable){return pair[1];}
            }
            return(false);
        },

        insertAction: function(ev, action) {

            ev.preventDefault();
            var self = this;

            //Verfica o numero de actions
            if(self.selected.length == 11)
            {
                sweetAlert({
                    title: 'Oops! Noob!',
                    text: 'Tem muita action nessa macro aí! :)',
                    type: 'error',
                    confirmButtonColor: 'red'
                });

                return;
            }

            var index = self.selected.indexOf(action);
            var is_default = 0;

            if(index < 0)
            {
                var next = self.selected.length + 1;
                if(self.selected.length < 1)
                {
                    is_default = 1;
                }
                self.selected.push({action: action, idx: next, target: '', wait: '', default: is_default});
                self.tpcost += parseInt(action.cost);
            }
        },

        removeAction: function(ev, action){

            ev.preventDefault();
            var self = this;

            var index = self.selected.indexOf(action);

            if(index > -1)
            {
                self.selected.$remove(action);
                self.tpcost -= parseInt(action.action.cost);
            }
        },

        makeMacro: function() {
            var self = this;

            if(self.macros.length == 0 && self.selected.length == 0)
            {
                sweetAlert({
                    title: 'E aí noob!?',
                    text: 'Tá vendo alguma action selecionada por acaso? :D',
                    type: 'error',
                    confirmButtonColor: 'red'
                });
            }

            self.$set('macros', []);

            self.selected.forEach(function(item){

                if(item.default)
                {
                    self.macros.push({text: '/macroicon "'+ item.action.name + '"', action: item });
                }

                var wait = (item.wait == '') ? '' : ' <wait.' + item.wait + '>';
                self.macros.push({text: '/ac "'+ item.action.name + '" '+ item.target + wait, action: item });

            });

        }

    },

    ready: function(){

        var self = this;

        self.load();

    }
});