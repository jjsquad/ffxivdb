@extends('layouts.app')

@section('styles')

@endsection

@section('content')

    <div class="container" id="actionsApp">
        @if(count($results) > 0)
            @foreach($results as $result)
                <a href="/character/profile/{{ $result['id'] }}">
                <img src="{{ $result['avatar'] }}" alt=""><br>
                {{ $result['name'] }}<br>
                </a>
                <hr>
            @endforeach
        @endif
    </div>

    <a href="/">Home</a>

@endsection