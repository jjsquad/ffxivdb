@extends('layouts.app')

@section('styles')

    <style>
        .field-bg { background-color: #292929; }
        body { color: white; }
        .field { float: right; }
        .emblem { position: absolute; right: 35px; margin-top: 5px; }
        .inactive { opacity: 0.2; }
        .focus { margin-left: 2px; margin-right: 2px; }
    </style>

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="text-center ffxiv-title">Bem-Vindo à MASTERY FreeCompany [MTR]</h3>
                    <hr>
                    <img src="{{ $fc->emblum[0] }}" class="emblem">
                    <img src="{{ $fc->emblum[1] }}" class="emblem">
                    <img src="{{ $fc->emblum[2] }}" class="emblem">
                    <table class="table table-bordered table-responsive">
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Nome:</span></td>
                            <td><span>{{ $fc->name  }} [{{ $fc->tag }}]</span></td>
                            <td width="15%" rowspan="10"></td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Formação:</span></td>
                            <td><span>{{ date('d/m/Y', $fc->formed) }}</span></td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Membros Ativos:</span></td>
                            <td><span>{{ $fc->memberCount }}</span></td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Slogan:</span></td>
                            <td><span>{{ $fc->slogan }}</span></td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Rank:</span></td>
                            <td><span>{{ $fc->ranking['current'] }}</span></td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Ranking:</span></td>
                            <td><span>Rank Semanal: {{ $fc->ranking['weekly'] }}<br>
                                      Rank Mensal: {{ $fc->ranking['monthly'] }}</span></td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Focus:</span></td>
                            <td>
                                @for($i = 0; $i < count($fc->focus); $i++)
                                    <img src="{!! $fc->focus[$i]['icon'] !!}" title="{{ $fc->focus[$i]['name'] }}" class="focus {{ ($fc->focus[$i]['active']) ? '' : 'inactive' }}">
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Roles:</span></td>
                            <td>
                                @for($i = 0; $i < count($fc->roles); $i++)
                                    <img src="{!! $fc->roles[$i]['icon'] !!}" title="{{ $fc->roles[$i]['name'] }}" class="focus {{ ($fc->roles[$i]['active']) ? '' : 'inactive' }}">
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Ativa:</span></td>
                            <td><span>{{ $fc->active }}</span></td>
                        </tr>
                        <tr>
                            <td class="field-bg" width="30%"><span class="ffxiv-title field">Recrutamento:</span></td>
                            <td><span>{{ $fc->recruitment }}</span></td>
                        </tr>
                    </table>
                    <div class="text-center" style="margin: 0; padding: 0;">
                        <small style="color: white">®2016 JORGE 'POPYS' GONÇALVES - LALAFELL :-)</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
