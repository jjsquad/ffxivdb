@extends('layouts.app')

@section('styles')

@endsection

@section('content')
    <div class="container" id="actionsApp">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="ffxiv-title">
                            <h4 class="text-center">SELECIONE UMA CLASSE/JOB</h4>
                            <div class="text-center">
                                <div class="classjob" v-for="classjob in classjobs" v-on:click="selectClass(classjob)">
                                    <img v-bind:src="'http://xivdb.com' + classjob.icon1" title="[@{{ classjob.abbr }}] @{{ classjob.name }}">
                                    @{{ classjob.abbr }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                {{--PASSO 1--}}
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="ffxiv-title">
                            <h4 class="text-center">PASSO 1: <small>SELECIONAR AS ACTIONS</small></h4>
                            <hr>
                        </div>
                        <div class="ffxiv-title">
                            <h3 class="text-center" v-if="!actions.length">SELECIONE UMA CLASSE/JOB PARA LISTAR AS ACTIONS</h3>
                            <h3 class="text-center" v-if="actions.length">@{{ currentSelected }}</h3>
                        </div>
                        <div class="actions">
                            <div v-for="action in actions" v-if="actions.length">
                                <div class="link tooltip2" v-on:click="insertAction($event,action)">
                                    <img v-bind:src="'http://xivdb.com/'+ action.icon">
                                    <div class="help">
                                        <action_level class="level">@{{ action.level }}</action_level>
                                        <img v-bind:src="'http://xivdb.com/'+ action.icon ">
                                        <strong>@{{ action.name }}</strong><small class="cost">COST: @{{ action.cost }}</small>
                                        <hr>
                                        <p class="tooltip2">@{{{ action.help_html }}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    {{--PASSO 3--}}
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="ffxiv-title">
                                    <h4 class="text-center">PASSO 3: <small>COPIAR O SCRIPT DA MACRO</small></h4>
                                    <hr>
                                </div>
                                <div class="col-md-12 text-center well-sm">
                                    <button class="btn btn-success btn-sm" v-on:click="makeMacro">GERAR MACRO</button>
                                </div>
                                <div class="ffxiv-macro">
                                    <div v-for="macro in macros">
                                        <span>@{{ macro.text }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--PASSO 2--}}
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="ffxiv-title">
                            <h4 class="text-center">PASSO 2: <small>AJUSTAR TARGET E TEMPO DE ESPERA (WAIT)</small></h4>
                            <hr>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="ffxiv-tpcost" style="padding-bottom: 5px; border-bottom: 1px dashed white">
                                    <h3>CUSTO TOTAL: <span>@{{ tpcost }}</span> TP/CP</h3>
                                    <h5 style="color: white; margin: 0; padding: 0">@{{ selected.length }} de 11 actions selecionadas</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div v-for="action in selected">
                                    <div class="selected">
                                        <div class="list-selected tooltip2">
                                            <a href="javascript:void(0)">
                                                <img v-bind:src="'http://xivdb.com'+ action.action.icon" >
                                                <div class="help">
                                                    <action_level class="level">@{{ action.action.level }}</action_level>
                                                    <img v-bind:src="'http://xivdb.com' + action.action.icon">
                                                    <strong>@{{ action.action.name }}</strong><small class="cost">COST: @{{ action.action.cost }}</small>
                                                    <hr>
                                                    <p class="tooltip2">@{{{ action.action.help_html }}}</p>
                                                </div>
                                            </a>
                                        </div>
                                        <span class="ffxiv-title">@{{ action.action.name }}</span>
                                        <i class="fa fa-fw fa-remove fa-3x" v-on:click="removeAction($event, action)"></i>
                                        <div class="target-group">
                                            <div class="target">
                                                <input maxlength="3" type="text" class="wait" v-model="action.wait">
                                                <img src="/images/cooldown_icon.png" width="30" height="30" title="< wait > : WAIT TIME">
                                            </div>
                                            <div class="target target-toggle" v-bind:class="{ active : action.target == '<me>' }">
                                                <img src="/images/060768.png" width="30" height="30" title="< me > : SELF TARGET" v-on:click="action.target='<me>'">
                                            </div>
                                            <div class="target target-toggle" v-bind:class="{ active : action.target == '<t>' }">
                                                <img src="/images/061710.png" width="30" height="30" title="< t > : TARGET" v-on:click="action.target='<t>'">
                                            </div>
                                            <div class="target target-toggle" v-bind:class="{ active : action.target == '<mo>' }">
                                                <img src="/images/heavy_dot_blue.png" width="30" height="30" title="< mo > : MOUSE TARGET" v-on:click="action.target='<mo>'">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center" style="margin: 0; padding: 0;">
                            <small style="color: white">®2016 JORGE 'POPYS' GONÇALVES - LALAFELL :-)</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection